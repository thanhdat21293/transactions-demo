# Transactions DEMO

## SERVER

- Koajs

Data from: [http://www.mockaroo.com/](http://www.mockaroo.com/)


## CLIENT

- Vue.js

# API

### GET `/list-money-packages`

- Lấy tất cả gói tiền có trong tất cả các đơn

Input: Rỗng

Output:

```json
[
  10000000,
  20000000,
  30000000,
  50000000,
  100000000,
  200000000,
  300000000
]
```

### GET `/list-time-by-money-package/:money_package`

- Lấy thời gian + đơn vị thời gian theo gói tiền

Input: 1 param

Output: 

```json
{
    "ngay": [
        {
            "time": 11,
            "time_unit": "ngay"
        }
    ],
    "tuan": [
        {
            "time": 9,
            "time_unit": "tuan"
        }
    ],
    "thang": [
        {
            "time": 10,
            "time_unit": "thang"
        }
    ],
    "nam": []
}
```

### GET `/search-loan-items/:location/:money_package/:time/:time_unit`

- Lấy loan_items theo gói tiền + thời gian + đơn vị thời gian + địa điểm

- location: nếu là 'all' thì sẽ tìm kiếm tất cả, ngược lại tìm kiếm theo vùng miền đã chọn

Input: 4 params

Output: 

```json 
{
    "items": [
        {
            "id": "lkeif09",
            "rate_min": 3.41,
            "rate_max": 29.62,
            "sale_fullname": "Natalya Boskell",
            "sale_avatar": "user_5.png",
            "bank_name": "Maritime Bank",
            "interest_total_min": 4092000,
            "interest_total_max": 35544000
        }
    ],
    "count": 1,
    "locations": [
        {
            "id": "HAN",
            "name": "Hà Nội"
        },
        {
            "id": "HCM",
            "name": "Hồ Chí Minh"
        },
        {
            "id": "DAN",
            "name": "Đà Nẵng"
        },
        {
            "id": "DAL",
            "name": "Đà Lạt"
        },
        {
            "id": "CAM",
            "name": "Cà Mau"
        },
        {
            "id": "NHT",
            "name": "Nha Trang"
        },
        {
            "id": "HAD",
            "name": "Hải Dương"
        },
        {
            "id": "TNG",
            "name": "Thái Nguyên"
        }
    ],
    "getLocaiton": {
        "id": "CAM",
        "name": "Cà Mau"
    }
}
```

### GET `/loan-item/:id`

- Lấy loan_item theo id

Input: 1 param

Output: 

```json
{
  "id": "lkeif09",
  "money_package": 30000000,
  "rate_min": 3.41,
  "rate_max": 29.62,
  "time": 4,
  "time_unit": "nam",
  "interest_per_month_min": 1023000,
  "interest_per_month_max": 8886000,
  "interest_total_min": 4092000,
  "interest_total_max": 35544000,
  "pay_total_min": 34092000,
  "pay_total_max": 65544000,
  "sale_fullname": "Natalya Boskell",
  "avatar": "user_5.png",
  "position": "Nhân viên kinh doanh",
  "contact_total": 54,
  "bank_name": "Maritime Bank",
  "location_name": "Cà Mau"
}
```
exports.up = function(knex, Promise) {
    return knex.schema
    .withSchema('public')
    .createTable('loan_items', (table) => {
        table.specificType('id', 'char(7)').notNullable().unique().notNullable().primary()
        table.enum('type', ['Vay tin chap']).notNullable()
        table.integer('money_package').notNullable()
        table.integer('time').notNullable()
        table.enum('time_unit', ['day', 'week', 'month', 'year']).notNullable()
        table.specificType('rate_min', 'decimal(5, 2) CHECK (rate_min >= 0 AND rate_min <= 100)').notNullable() // Giá trị từ 0.00 den 100.00, VD: 1.23, 66.32
        table.specificType('rate_max', 'decimal(5, 2) CHECK (rate_max >= 0 AND rate_max <= 100 AND rate_max >= rate_min)').notNullable() // Giá trị từ 0.00 den 100.00, VD: 1.23, 66.32
        table.integer('sale_id').notNullable().references('id').inTable('sales')
        table.integer('bank_id').notNullable().references('id').inTable('banks')
        table.specificType('location_id', 'char(3)').notNullable().references('id').inTable('locations')
        table.enum('status', ['finish', 'active', 'pending', 'cancel']).notNullable().defaultTo('pending')
        table.timestamp('started_at').notNullable()
        table.timestamp('expired_at').notNullable()
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('loan_items')
};
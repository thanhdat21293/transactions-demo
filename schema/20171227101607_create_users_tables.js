exports.up = function(knex, Promise) {
    return knex.schema
    .withSchema('public')
    .createTable('users', (table) => {
        table.increments()
        table.string('fullname', 50).notNullable()
        table.specificType('username', 'char(30)').unique().notNullable()
        table.specificType('email', 'char(80)').unique().notNullable()
        table.specificType('hash', 'char(32)').notNullable() // Mat khau md5
        table.specificType('salt', 'char(5)').notNullable()
        table.specificType('avatar', 'char(50)').defaultTo('avatar.png')
        table.specificType('phone', 'char(12)')
        table.enum('position', ['Executive', 'CEO', 'Employees']).defaultTo('Employees')
        table.specificType('company_id', 'char(10)').references('id').inTable('companies')
        table.enum('status', ['active', 'pending', 'banned']).notNullable().defaultTo('pending')
        table.timestamp('created_at').defaultTo(knex.fn.now())
        table.timestamp('updated_at').defaultTo(knex.fn.now())
    })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('users')
};
exports.up = function(knex, Promise) {
    return knex.schema
    .withSchema('public')
    .createTable('locations', (table) => {
        table.specificType('id', 'char(3)').unique().primary() // 'HCM', 'HN'
        table.string('name', 30).notNullable() // 'TP Ho Chi minh', 'TP Ha Noi'
    })
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTableIfExists('locations')
};
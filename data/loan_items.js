module.exports = [
    {
    "id": "923kejf",
    "type": "Vay tín chấp",
    "money_package": 20000000,
    "time": 10,
    "time_unit": "tuan",
    "rate_min": 13.45,
    "rate_max": 25.01,
    "sale_id": 12,
    "bank_id": 1,
    "location_id": "DAL",
    "status": "active",
    "started_at": "2017-01-18T10:48:52Z",
    "expired_at": "2017-04-21T16:02:02Z",
    "created_at": "2017-09-09T10:04:36Z",
    "updated_at": "2017-12-20T17:42:41Z"
  }, {
    "id": "h9kdm3r",
    "type": "Vay tín chấp",
    "money_package": 50000000,
    "time": 11,
    "time_unit": "ngay",
    "rate_min": 6.5,
    "rate_max": 21.89,
    "sale_id": 15,
    "bank_id": 2,
    "location_id": "HAD",
    "status": "pending",
    "started_at": "2017-05-11T14:44:55Z",
    "expired_at": "2017-10-21T22:04:17Z",
    "created_at": "2017-07-24T20:23:23Z",
    "updated_at": "2017-12-03T18:24:19Z"
  }, {
    "id": "lei30dk",
    "type": "Vay tín chấp",
    "money_package": 50000000,
    "time": 10,
    "time_unit": "thang",
    "rate_min": 19.52,
    "rate_max": 23.67,
    "sale_id": 20,
    "bank_id": 3,
    "location_id": "HAD",
    "status": "active",
    "started_at": "2017-01-27T21:26:28Z",
    "expired_at": "2017-10-26T23:22:13Z",
    "created_at": "2017-10-08T02:27:55Z",
    "updated_at": "2017-03-24T17:58:50Z"
  }, {
    "id": "eo3jf8f",
    "type": "Vay tín chấp",
    "money_package": 300000000,
    "time": 3,
    "time_unit": "tuan",
    "rate_min": 9.34,
    "rate_max": 23.88,
    "sale_id": 1,
    "bank_id": 4,
    "location_id": "DAN",
    "status": "pending",
    "started_at": "2017-09-20T11:16:28Z",
    "expired_at": "2017-12-18T10:00:23Z",
    "created_at": "2017-09-24T16:39:57Z",
    "updated_at": "2017-10-25T22:51:51Z"
  }, {
    "id": "iejf033",
    "type": "Vay tín chấp",
    "money_package": 30000000,
    "time": 4,
    "time_unit": "thang",
    "rate_min": 18.86,
    "rate_max": 26.64,
    "sale_id": 17,
    "bank_id": 5,
    "location_id": "NHT",
    "status": "active",
    "started_at": "2017-04-30T01:31:01Z",
    "expired_at": "2017-09-08T09:33:31Z",
    "created_at": "2017-10-04T22:17:52Z",
    "updated_at": "2017-04-06T23:14:37Z"
  }, {
    "id": "leofek9",
    "type": "Vay tín chấp",
    "money_package": 50000000,
    "time": 9,
    "time_unit": "tuan",
    "rate_min": 3.03,
    "rate_max": 25.62,
    "sale_id": 18,
    "bank_id": 6,
    "location_id": "HAN",
    "status": "active",
    "started_at": "2017-06-15T21:27:27Z",
    "expired_at": "2017-03-23T20:16:20Z",
    "created_at": "2017-10-02T02:28:53Z",
    "updated_at": "2017-07-25T09:38:44Z"
  }, {
    "id": "woekmr9",
    "type": "Vay tín chấp",
    "money_package": 200000000,
    "time": 12,
    "time_unit": "nam",
    "rate_min": 18.91,
    "rate_max": 24.19,
    "sale_id": 9,
    "bank_id": 7,
    "location_id": "HCM",
    "status": "cancel",
    "started_at": "2017-10-03T07:08:06Z",
    "expired_at": "2017-07-31T08:55:09Z",
    "created_at": "2017-06-08T23:50:06Z",
    "updated_at": "2017-07-07T09:06:15Z"
  }, {
    "id": "lkeif09",
    "type": "Vay tín chấp",
    "money_package": 30000000,
    "time": 4,
    "time_unit": "nam",
    "rate_min": 3.41,
    "rate_max": 29.62,
    "sale_id": 5,
    "bank_id": 8,
    "location_id": "CAM",
    "status": "cancel",
    "started_at": "2017-05-08T11:18:51Z",
    "expired_at": "2017-12-15T03:45:27Z",
    "created_at": "2017-10-04T12:33:44Z",
    "updated_at": "2017-12-26T18:09:11Z"
  }, {
    "id": "nvbw88e",
    "type": "Vay tín chấp",
    "money_package": 30000000,
    "time": 4,
    "time_unit": "nam",
    "rate_min": 4.18,
    "rate_max": 22.22,
    "sale_id": 2,
    "bank_id": 9,
    "location_id": "HAD",
    "status": "finish",
    "started_at": "2017-04-02T07:19:28Z",
    "expired_at": "2017-12-01T16:40:46Z",
    "created_at": "2017-07-03T07:04:30Z",
    "updated_at": "2017-10-20T20:13:42Z"
  }, {
    "id": "lekrui0",
    "type": "Vay tín chấp",
    "money_package": 100000000,
    "time": 12,
    "time_unit": "thang",
    "rate_min": 19.13,
    "rate_max": 22.65,
    "sale_id": 5,
    "bank_id": 10,
    "location_id": "NHT",
    "status": "cancel",
    "started_at": "2017-08-01T23:33:29Z",
    "expired_at": "2017-02-24T07:56:33Z",
    "created_at": "2017-07-10T23:50:40Z",
    "updated_at": "2017-03-20T03:46:25Z"
  }, {
    "id": "qofk99e",
    "type": "Vay tín chấp",
    "money_package": 10000000,
    "time": 5,
    "time_unit": "nam",
    "rate_min": 13.69,
    "rate_max": 24.69,
    "sale_id": 20,
    "bank_id": 11,
    "location_id": "HCM",
    "status": "pending",
    "started_at": "2017-11-26T12:25:32Z",
    "expired_at": "2017-05-24T01:43:11Z",
    "created_at": "2017-06-20T02:22:07Z",
    "updated_at": "2017-06-21T08:00:29Z"
  }, {
    "id": "rorpss9",
    "type": "Vay tín chấp",
    "money_package": 200000000,
    "time": 1,
    "time_unit": "nam",
    "rate_min": 7.37,
    "rate_max": 26.25,
    "sale_id": 17,
    "bank_id": 1,
    "location_id": "HAD",
    "status": "active",
    "started_at": "2017-08-14T17:52:33Z",
    "expired_at": "2017-08-30T20:03:55Z",
    "created_at": "2017-03-10T19:53:41Z",
    "updated_at": "2017-04-22T15:26:05Z"
  }, {
    "id": "pocmn00",
    "type": "Vay tín chấp",
    "money_package": 300000000,
    "time": 7,
    "time_unit": "tuan",
    "rate_min": 15.66,
    "rate_max": 27.5,
    "sale_id": 19,
    "bank_id": 2,
    "location_id": "HAD",
    "status": "cancel",
    "started_at": "2017-09-28T12:22:15Z",
    "expired_at": "2017-07-25T09:09:51Z",
    "created_at": "2017-10-21T14:25:48Z",
    "updated_at": "2017-09-14T10:44:36Z"
  }, {
    "id": "irjaerl",
    "type": "Vay tín chấp",
    "money_package": 200000000,
    "time": 2,
    "time_unit": "thang",
    "rate_min": 16.77,
    "rate_max": 22.45,
    "sale_id": 10,
    "bank_id": 3,
    "location_id": "HAD",
    "status": "cancel",
    "started_at": "2017-07-07T11:45:27Z",
    "expired_at": "2017-04-08T01:06:59Z",
    "created_at": "2017-02-28T05:31:32Z",
    "updated_at": "2017-03-04T02:10:41Z"
  }, {
    "id": "nvhdeoo",
    "type": "Vay tín chấp",
    "money_package": 20000000,
    "time": 12,
    "time_unit": "thang",
    "rate_min": 8.78,
    "rate_max": 27.68,
    "sale_id": 3,
    "bank_id": 4,
    "location_id": "DAL",
    "status": "cancel",
    "started_at": "2017-01-01T08:52:10Z",
    "expired_at": "2017-07-26T11:00:42Z",
    "created_at": "2017-02-28T13:48:57Z",
    "updated_at": "2017-01-11T21:07:58Z"
  }, {
    "id": "lfmmd0g",
    "type": "Vay tín chấp",
    "money_package": 30000000,
    "time": 6,
    "time_unit": "nam",
    "rate_min": 18.39,
    "rate_max": 29.28,
    "sale_id": 20,
    "bank_id": 5,
    "location_id": "DAL",
    "status": "pending",
    "started_at": "2017-12-11T13:29:59Z",
    "expired_at": "2017-12-21T00:14:06Z",
    "created_at": "2017-08-12T03:57:21Z",
    "updated_at": "2017-10-28T04:44:36Z"
  }, {
    "id": "jmndbbd",
    "type": "Vay tín chấp",
    "money_package": 300000000,
    "time": 4,
    "time_unit": "nam",
    "rate_min": 9.0,
    "rate_max": 24.39,
    "sale_id": 11,
    "bank_id": 6,
    "location_id": "HAD",
    "status": "cancel",
    "started_at": "2017-04-21T22:57:21Z",
    "expired_at": "2017-08-14T01:14:10Z",
    "created_at": "2017-12-16T20:43:35Z",
    "updated_at": "2017-09-16T05:14:22Z"
  }, {
    "id": "t23mmss",
    "type": "Vay tín chấp",
    "money_package": 300000000,
    "time": 6,
    "time_unit": "thang",
    "rate_min": 17.47,
    "rate_max": 23.94,
    "sale_id": 19,
    "bank_id": 7,
    "location_id": "TNG",
    "status": "pending",
    "started_at": "2017-01-23T04:58:36Z",
    "expired_at": "2017-08-15T03:50:49Z",
    "created_at": "2017-07-30T05:07:05Z",
    "updated_at": "2017-03-31T05:45:50Z"
  }, {
    "id": "lsdkfef",
    "type": "Vay tín chấp",
    "money_package": 30000000,
    "time": 1,
    "time_unit": "thang",
    "rate_min": 2.03,
    "rate_max": 25.22,
    "sale_id": 4,
    "bank_id": 8,
    "location_id": "DAN",
    "status": "pending",
    "started_at": "2017-08-01T07:31:58Z",
    "expired_at": "2017-10-31T07:58:06Z",
    "created_at": "2017-06-22T08:03:46Z",
    "updated_at": "2017-09-06T20:47:52Z"
  }, {
    "id": "lei303f",
    "type": "Vay tín chấp",
    "money_package": 200000000,
    "time": 5,
    "time_unit": "nam",
    "rate_min": 16.31,
    "rate_max": 24.75,
    "sale_id": 12,
    "bank_id": 9,
    "location_id": "TNG",
    "status": "pending",
    "started_at": "2017-05-20T18:12:22Z",
    "expired_at": "2017-08-13T02:30:37Z",
    "created_at": "2017-10-10T21:45:33Z",
    "updated_at": "2017-03-20T08:05:32Z"
  }
]
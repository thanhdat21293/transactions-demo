module.exports = [
	{
		"id": "HAN",
		"name": "Hà Nội"
	},
	{
		"id": "HCM",
		"name": "Hồ Chí Minh"
  },
	{
		"id": "DAN",
		"name": "Đà Nẵng"
  },
	{
		"id": "DAL",
		"name": "Đà Lạt"
  },
	{
		"id": "CAM",
		"name": "Cà Mau"
  },
	{
		"id": "NHT",
		"name": "Nha Trang"
  },
	{
		"id": "HAD",
		"name": "Hải Dương"
  },
	{
		"id": "TNG",
		"name": "Thái Nguyên"
  }
]
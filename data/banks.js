module.exports = [
    {
        id: 1,
        name: 'ACB'
    },
    {
        id: 2,
        name: 'TPBank'
    },
    {
        id: 3,
        name: 'DAF'
    },
    {
        id: 4,
        name: 'SeABank'
    },
    {
        id: 5,
        name: 'ABBANK'
    },
    {
        id: 6,
        name: 'BacABank'
    },
    {
        id: 7,
        name: 'VietCapitalBank'
    },
    {
        id: 8,
        name: 'Maritime Bank'
    },
    {
        id: 9,
        name: 'Techcombank'
    },
    {
        id: 10,
        name: 'KienLongBank'
    },
    {
        id: 11,
        name: 'Nam A Bank'
    }
]
module.exports = {
  convertTimeUnit: timeUnit => {
    if (timeUnit === "nam") {
      return "năm";
    } else if (timeUnit === "thang") {
      return "tháng";
    } else if (timeUnit === "tuan") {
      return "tuần";
    } else if (timeUnit === "ngay") {
      return "ngày";
    } else {
      return "";
    }
  },
  numberFormat: number => {
    return new Intl.NumberFormat(['ban', 'id']).format(number)
  }
}
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'


import './assets/css/style.css'
import './assets/css/style1.css'

Vue.config.productionTip = false
const f = require('./filters/filters');
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  filters: f,
  template: '<App/>',
  components: { App }
})

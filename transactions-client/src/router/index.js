import Vue from 'vue'
import Router from 'vue-router'
import contentIframe from '@/components/content_iframe'
import LoanItemDetail from '@/components/loanDetail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'contentIframe',
      component: contentIframe
    },
    {
      path: '/loan-detail/:id',
      name: 'LoanItemDetail',
      component: LoanItemDetail
    }
    
  ]
})

/* 
10000000,20000000,30000000,50000000,100000000,200000000,300000000,500000000
HAN,HCM,DAN,DAL,CAM,NHT,HAD,TNG
finish,active,pending,cancel
lei303f,lsdkfef,t23mmss,jmndbbd,lfmmd0g,nvhdeoo,irjaerl,pocmn00,rorpss9,qofk99e,lekrui0,nvbw88e,lkeif09,woekmr9,leofek9,iejf033,eo3jf8f,lei30dk,h9kdm3r,923kejf
finish,active,deal_sale,deal_user,pending,cancel
*/
const Koa = require('koa');
const Router = require('koa-router');

const app = new Koa();
const bodyParser = require('koa-bodyparser');
const logger = require('koa-logger')
var router;
if(process.env.NODE_ENV === 'production') {
	 router = Router({prefix: '/api'});
}else {
	 router = Router();
}

// Database
const Locations = require('./data/locations')
const Companies = require('./data/companies')
const Users = require('./data/users')
const Banks = require('./data/banks')
const Sales = require('./data/sales')
const LoanItems = require('./data/loan_items')
const LoanRequests = require('./data/loan_requests')
const LoanItemViews = require('./data/loan_item_views')
const UserLoanItemViews = require('./data/user_loan_item_views')
app.use(logger())
app.use(bodyParser());
app.use(async (ctx, next) => {
  ctx.set("Access-Control-Allow-Origin", "*");
	ctx.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	await next()
});
// Lấy gói tiền 
router.get('/list-money-packages', async ctx => {
		let listMoneyPackages = LoanItems
																	.map(item => item.money_package)
																	.filter((value, index, self) => self.indexOf(value) === index)
																	.sort((a,b) => { return a - b })
		ctx.body = listMoneyPackages
	
});

// Từ gói tiền chọn, lấy thời gian + đơn vị thời gian theo gói tiền
router.get('/list-time-by-money-package/:money_package', async ctx => {
	let money_package = parseInt(ctx.params.money_package)
	let listTimeByMoneyPackage = LoanItems
																		.filter(item => item.money_package === money_package)
																		.map(item => {
																			return {
																				time: item.time,
																				time_unit: item.time_unit
																			}
																		})
																		.sort((a,b) => { return a.time - b.time })
	if(listTimeByMoneyPackage.length > 0) {
		let listTimeByMoneyPackage1 = [listTimeByMoneyPackage[0]]
		listTimeByMoneyPackage.map(item => {
			let check = 1;
			listTimeByMoneyPackage1.map(item1 => {
				if(item.time === item1.time && item.time_unit === item1.time_unit) {
					check = 0
					return;
				}
			})
			if(check === 1) {
				listTimeByMoneyPackage1.push(item)
			}
		})
		let ngayByMoneypackage = listTimeByMoneyPackage1.filter(item => item.time_unit === 'ngay')
		let tuanByMoneypackage = listTimeByMoneyPackage1.filter(item => item.time_unit === 'tuan')
		let thangByMoneypackage = listTimeByMoneyPackage1.filter(item => item.time_unit === 'thang')
		let namByMoneypackage = listTimeByMoneyPackage1.filter(item => item.time_unit === 'nam')
		ctx.body = {
			ngay: ngayByMoneypackage,
			tuan: tuanByMoneypackage,
			thang: thangByMoneypackage,
			nam: namByMoneypackage
		}
	}else {
		ctx.body = {}
	}
})

// Từ gói tiền + thời gian => loan_items list
router.get('/search-loan-items/:location/:money_package/:time/:time_unit', async ctx => {
	let money_package = parseInt(ctx.params.money_package)
	let time = parseInt(ctx.params.time)
	let time_unit = ctx.params.time_unit
	let location = ctx.params.location

	let listTimeByMoneyPackage = []
	let getLocaiton = {
		id: 'all',
		name: 'all'
	}
	if(location === 'all') {
		listTimeByMoneyPackage = LoanItems
																		.filter(item => item.money_package === money_package && item.time === time && item.time_unit === time_unit)
	}else {
		listTimeByMoneyPackage = LoanItems
																		.filter(item => item.money_package === money_package && item.time === time && item.time_unit === time_unit && item.location_id === location)
		getLocaiton = Locations.find(item => item.id === location)
	}

	listTimeByMoneyPackage = listTimeByMoneyPackage.map(item => {
																								let getSale = Sales.find(sale => sale.id === item.sale_id)
																								let getBank = Banks.find(bank => bank.id === item.bank_id)
																								
																								let interest_per_month_min = (item.money_package * item.rate_min)/100
																								let interest_per_month_max = (item.money_package * item.rate_max)/100
																								let interest_total_min = interest_per_month_min * item.time
																								let interest_total_max = interest_per_month_max * item.time
																								return {
																									id: item.id,
																									rate_min: item.rate_min,
																									rate_max: item.rate_max,
																									sale_fullname: getSale.fullname,
																									sale_avatar: getSale.avatar,
																									bank_name: getBank.name,
																									interest_total_min,
																									interest_total_max
																								}
																							})
	
	ctx.body = {
		items: listTimeByMoneyPackage,
		count: listTimeByMoneyPackage.length,
		locations: Locations,
		getLocaiton
	}
})

// Chi tiết loan_item 
router.get('/loan-item/:id', async ctx => {
	let id = ctx.params.id
	let loanItem = LoanItems.find(item => item.id === id) || {}
	let result = {}
	if (loanItem.id) {
		let getSale = Sales.find(sale => sale.id === loanItem.sale_id)
		let getBank = Banks.find(bank => bank.id === loanItem.bank_id)
		let getLocation = Locations.find(location => location.id === loanItem.location_id)

		// Lấy tổng số lần liên hệ của sales
		let sale_contact_total = LoanRequests
																				.map(item => {
																					let loanItemsById = LoanItems.filter(loanItem => loanItem.id === item.loan_item_id && loanItem.sale_id === 4)
																																				.map(loanItem => loanItem.sale_id)
																					return loanItemsById
																				}).join("").split("")

		// Lưu vào biến để tái sử dụng 
		let interest_per_month_min = (loanItem.money_package * loanItem.rate_min)/100
		let interest_per_month_max = (loanItem.money_package * loanItem.rate_max)/100

		result.id = loanItem.id
		result.money_package = loanItem.money_package
		result.rate_min = loanItem.rate_min
		result.rate_max = loanItem.rate_max
		result.time = loanItem.time
		result.time_unit = loanItem.time_unit
		result.interest_per_month_min = interest_per_month_min
		result.interest_per_month_max = interest_per_month_max
		result.interest_total_min = interest_per_month_min * loanItem.time
		result.interest_total_max = interest_per_month_max * loanItem.time
		result.pay_total_min = loanItem.money_package + interest_per_month_min * loanItem.time
		result.pay_total_max = loanItem.money_package + interest_per_month_max * loanItem.time
		// sales
		result.sale_fullname = getSale.fullname
		result.avatar = getSale.avatar
		result.position = getSale.position
		result.contact_total = sale_contact_total.length
		// Banks
		result.bank_name = getBank.name
		// Locations
		result.location_name = getLocation.name
	}
	ctx.body = result
})

app.use(router.routes())
app.use(router.allowedMethods())

const port = 10003
app.listen(port, () => {
  console.log('Start http://localhost:' + port)
});